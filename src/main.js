import Vue from 'vue'
import App from './App.vue'

import StartScreen from './components/StartScreen.vue';
import Question from './components/Question.vue';
// import Result from './components/Result.vue';
import Message from './components/Message.vue';
// import Status from './components/Status.vue';
import InputField from './components/InputField.vue';

Vue.component('StartScreen', StartScreen);
Vue.component('Question', Question);
// Vue.component('Result', Result);
Vue.component('Message', Message);
// Vue.component('Status', Status);
Vue.component('InputField', InputField);

new Vue({
  el: '#app',
  render: h => h(App)
})
